﻿namespace dataSet1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAccnt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBlnc = new System.Windows.Forms.TextBox();
            this.cmdInsertDs = new System.Windows.Forms.Button();
            this.cmdUpdateDs = new System.Windows.Forms.Button();
            this.cmdDeleteDs = new System.Windows.Forms.Button();
            this.cmdUpdateDb = new System.Windows.Forms.Button();
            this.cmdExit = new System.Windows.Forms.Button();
            this.dg1 = new System.Windows.Forms.DataGridView();
            this.lblRowState = new System.Windows.Forms.Label();
            this.lblRowIndex = new System.Windows.Forms.Label();
            this.lblUpdatedRows = new System.Windows.Forms.Label();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dgView = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dg1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Account (1 to 100)";
            // 
            // txtAccnt
            // 
            this.txtAccnt.Location = new System.Drawing.Point(28, 25);
            this.txtAccnt.Name = "txtAccnt";
            this.txtAccnt.Size = new System.Drawing.Size(165, 20);
            this.txtAccnt.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "First Name:";
            // 
            // txtFName
            // 
            this.txtFName.Location = new System.Drawing.Point(28, 70);
            this.txtFName.Name = "txtFName";
            this.txtFName.Size = new System.Drawing.Size(165, 20);
            this.txtFName.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Last Name:";
            // 
            // txtLName
            // 
            this.txtLName.Location = new System.Drawing.Point(28, 122);
            this.txtLName.Name = "txtLName";
            this.txtLName.Size = new System.Drawing.Size(165, 20);
            this.txtLName.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Balance:";
            // 
            // txtBlnc
            // 
            this.txtBlnc.Location = new System.Drawing.Point(28, 174);
            this.txtBlnc.Name = "txtBlnc";
            this.txtBlnc.Size = new System.Drawing.Size(165, 20);
            this.txtBlnc.TabIndex = 7;
            // 
            // cmdInsertDs
            // 
            this.cmdInsertDs.Location = new System.Drawing.Point(28, 215);
            this.cmdInsertDs.Name = "cmdInsertDs";
            this.cmdInsertDs.Size = new System.Drawing.Size(165, 48);
            this.cmdInsertDs.TabIndex = 8;
            this.cmdInsertDs.Text = "Insert Row in DataSet";
            this.cmdInsertDs.UseVisualStyleBackColor = true;
            this.cmdInsertDs.Click += new System.EventHandler(this.cmdInsertDs_Click);
            // 
            // cmdUpdateDs
            // 
            this.cmdUpdateDs.Enabled = false;
            this.cmdUpdateDs.Location = new System.Drawing.Point(28, 269);
            this.cmdUpdateDs.Name = "cmdUpdateDs";
            this.cmdUpdateDs.Size = new System.Drawing.Size(165, 48);
            this.cmdUpdateDs.TabIndex = 9;
            this.cmdUpdateDs.Text = "Update Row in DataSet";
            this.cmdUpdateDs.UseVisualStyleBackColor = true;
            this.cmdUpdateDs.Click += new System.EventHandler(this.cmdUpdateDs_Click);
            // 
            // cmdDeleteDs
            // 
            this.cmdDeleteDs.Enabled = false;
            this.cmdDeleteDs.Location = new System.Drawing.Point(28, 324);
            this.cmdDeleteDs.Name = "cmdDeleteDs";
            this.cmdDeleteDs.Size = new System.Drawing.Size(165, 48);
            this.cmdDeleteDs.TabIndex = 10;
            this.cmdDeleteDs.Text = "Delete Row from DataSet";
            this.cmdDeleteDs.UseVisualStyleBackColor = true;
            this.cmdDeleteDs.Click += new System.EventHandler(this.cmdDeleteDs_Click);
            // 
            // cmdUpdateDb
            // 
            this.cmdUpdateDb.Location = new System.Drawing.Point(28, 378);
            this.cmdUpdateDb.Name = "cmdUpdateDb";
            this.cmdUpdateDb.Size = new System.Drawing.Size(165, 48);
            this.cmdUpdateDb.TabIndex = 11;
            this.cmdUpdateDb.Text = "Update Database";
            this.cmdUpdateDb.UseVisualStyleBackColor = true;
            this.cmdUpdateDb.Click += new System.EventHandler(this.cmdUpdateDb_Click);
            // 
            // cmdExit
            // 
            this.cmdExit.Location = new System.Drawing.Point(496, 380);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(165, 48);
            this.cmdExit.TabIndex = 12;
            this.cmdExit.Text = "Exit";
            this.cmdExit.UseVisualStyleBackColor = true;
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // dg1
            // 
            this.dg1.AllowUserToAddRows = false;
            this.dg1.AllowUserToDeleteRows = false;
            this.dg1.AllowUserToResizeColumns = false;
            this.dg1.AllowUserToResizeRows = false;
            this.dg1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg1.Location = new System.Drawing.Point(218, 56);
            this.dg1.Name = "dg1";
            this.dg1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg1.Size = new System.Drawing.Size(443, 294);
            this.dg1.TabIndex = 13;
            // 
            // lblRowState
            // 
            this.lblRowState.AutoSize = true;
            this.lblRowState.Location = new System.Drawing.Point(218, 9);
            this.lblRowState.Name = "lblRowState";
            this.lblRowState.Size = new System.Drawing.Size(0, 13);
            this.lblRowState.TabIndex = 14;
            // 
            // lblRowIndex
            // 
            this.lblRowIndex.AutoSize = true;
            this.lblRowIndex.Location = new System.Drawing.Point(218, 32);
            this.lblRowIndex.Name = "lblRowIndex";
            this.lblRowIndex.Size = new System.Drawing.Size(0, 13);
            this.lblRowIndex.TabIndex = 15;
            // 
            // lblUpdatedRows
            // 
            this.lblUpdatedRows.AutoSize = true;
            this.lblUpdatedRows.Location = new System.Drawing.Point(215, 353);
            this.lblUpdatedRows.Name = "lblUpdatedRows";
            this.lblUpdatedRows.Size = new System.Drawing.Size(0, 13);
            this.lblUpdatedRows.TabIndex = 16;
            // 
            // dgView
            // 
            this.dgView.AllowUserToAddRows = false;
            this.dgView.AllowUserToDeleteRows = false;
            this.dgView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgView.Location = new System.Drawing.Point(676, 56);
            this.dgView.Name = "dgView";
            this.dgView.ReadOnly = true;
            this.dgView.Size = new System.Drawing.Size(443, 294);
            this.dgView.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(679, 357);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Unchanged:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(679, 385);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Added:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(679, 413);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Modified:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(679, 441);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Deleted: ";
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(754, 357);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 15);
            this.label9.TabIndex = 22;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(754, 383);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 18);
            this.label10.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Lime;
            this.label11.Location = new System.Drawing.Point(754, 411);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 18);
            this.label11.TabIndex = 24;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(754, 439);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 17);
            this.label12.TabIndex = 25;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1154, 472);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dgView);
            this.Controls.Add(this.lblUpdatedRows);
            this.Controls.Add(this.lblRowIndex);
            this.Controls.Add(this.lblRowState);
            this.Controls.Add(this.dg1);
            this.Controls.Add(this.cmdExit);
            this.Controls.Add(this.cmdUpdateDb);
            this.Controls.Add(this.cmdDeleteDs);
            this.Controls.Add(this.cmdUpdateDs);
            this.Controls.Add(this.cmdInsertDs);
            this.Controls.Add(this.txtBlnc);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtLName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtFName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtAccnt);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "dataSet1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dg1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAccnt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtLName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBlnc;
        private System.Windows.Forms.Button cmdInsertDs;
        private System.Windows.Forms.Button cmdUpdateDs;
        private System.Windows.Forms.Button cmdDeleteDs;
        private System.Windows.Forms.Button cmdUpdateDb;
        private System.Windows.Forms.Button cmdExit;
        private System.Windows.Forms.DataGridView dg1;
        private System.Windows.Forms.Label lblRowState;
        private System.Windows.Forms.Label lblRowIndex;
        private System.Windows.Forms.Label lblUpdatedRows;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.DataGridView dgView;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
    }
}

