﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace dataSet1
{
    public partial class Form1 : Form
    {

        private SqlConnection conn = null;
        private SqlDataAdapter da = null;
        private DataSet ds = null;
        private int rowIndex = -1;


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            getData();
            dg1.Click += dg1_Click;
        }

        private void dg1_Click(object sender, EventArgs e)
        {
            //Capture rowIndex from dg1
            rowIndex = dg1.CurrentRow.Index;

            //Align rowIndex with the index of selection in Dataset
            //Iterate through the table to get the appropriate index
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i].RowState != DataRowState.Deleted)
                {
                    //If the selected account number is the same as the one in the dataset
                    if (dg1.CurrentRow.Cells[0].Value.ToString().Equals(ds.Tables[0].Rows[i].ItemArray[0].ToString()))
                    {
                        rowIndex = i;
                        break;
                    }
                }
            }

            //Add values from dataset to text fields
            txtAccnt.Text = dg1.CurrentRow.Cells[0].Value.ToString();
            txtFName.Text = dg1.CurrentRow.Cells[1].Value.ToString();
            txtLName.Text = dg1.CurrentRow.Cells[2].Value.ToString();
            txtBlnc.Text = dg1.CurrentRow.Cells[3].Value.ToString();

            //Set the values to row index and row state
            lblRowIndex.Text = "rowIndex = " + rowIndex;
            lblRowState.Text = "rowState = " + ds.Tables[0].Rows[rowIndex].RowState;

            setControleState("u/d");
        }

        private void getData() 
        {
            string connStr = "Data Source=(LocalDB)\\v11.0;AttachDbFilename=C:\\Users\\rafaelbattesti\\Source\\Repos\\dataSet1\\dataSet1\\Clients.mdf;Integrated Security=True";

            try
            {
                conn = new SqlConnection(connStr);
                conn.Open();
                string sql = "SELECT * FROM [tClients]";
                da = new SqlDataAdapter(sql, conn);
                SqlCommandBuilder cb = new SqlCommandBuilder(da);
                ds = new DataSet();
                da.Fill(ds, "tClients");
                conn.Close();

                //Bind And Display
                bindingSource1.DataSource = ds;
                bindingSource1.DataMember = "tClients";
                dg1.DataSource = bindingSource1;
                dg1.ClearSelection();

                //Create dataView
                DataView myView = new DataView(ds.Tables[0]);

                //Configure dataview to show deleted rows
                myView.RowStateFilter = DataViewRowState.CurrentRows | DataViewRowState.Deleted;

                //Bind dgView
                dgView.DataSource = myView;
                dgView.ClearSelection();

            }
            catch (SqlException ex)
            {
                if (conn != null) 
                {
                    conn.Close();
                    
                }
                MessageBox.Show(ex.Message, "Error Reading Data");
            }
        }

        private void cmdInsertDs_Click(object sender, EventArgs e)
        {
            if (dataGood())
            {
                if (isValidPrimaryKey("i"))
                {
                    //Create data row and populate with data from fields
                    DataRow dr = ds.Tables["tClients"].NewRow();
                    dr["Account"] = Convert.ToInt32(txtAccnt.Text);
                    dr["FirstName"] = txtFName.Text;
                    dr["LastName"] = txtLName.Text;
                    dr["Balance"] = Convert.ToDouble(txtBlnc.Text);

                    //Add row to the Tables collection in the DataSet
                    ds.Tables[0].Rows.Add(dr);
                    clearText();
                    formatGrid();
                }
            }
        }

        private void cmdUpdateDs_Click(object sender, EventArgs e)
        {
            if (dataGood())
            {
                if (isValidPrimaryKey("u"))
                {
                    DataRow dr = ds.Tables[0].Rows[rowIndex];
                    dr["Account"] = Convert.ToInt32(txtAccnt.Text);
                    dr["FirstName"] = txtFName.Text;
                    dr["LastName"] = txtLName.Text;
                    dr["Balance"] = Convert.ToDouble(txtBlnc.Text);
                    setControleState("i");
                    formatGrid();
                }
            }
        }

        private void cmdDeleteDs_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete this record?", "Confirm Record Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
            {
                //Delete Row
                ds.Tables[0].Rows[rowIndex].Delete();
                formatGrid();
            }
            setControleState("i");
        }

        private void cmdUpdateDb_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                int updatedRows = da.Update(ds, "tClients");
                conn.Close();

                lblUpdatedRows.Text = "Updated Rows: " + updatedRows;

                formatGrid();
            }
            catch (SqlException ex)
            {
                if (conn != null)
                {
                    conn.Close();
                }
                MessageBox.Show(ex.Message, "Error Updating Database");
            }
        }

        private void cmdExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void clearText()
        {
            txtFName.Text = "";
            txtLName.Text = "";
            txtBlnc.Text = "";
            txtAccnt.Text = "";
            txtAccnt.Focus();
            dg1.ClearSelection();
        }

        private bool dataGood()
        {
            return true;
        }

        private bool isValidPrimaryKey(string state)
        {
            //TODO
            return true;
        }

        private void setControleState(string state)
        {
            if (state.Equals("i"))
            {
                cmdInsertDs.Enabled = true;
                cmdUpdateDs.Enabled = false;
                cmdDeleteDs.Enabled = false;
                clearText();
            }
            else if (state.Equals("u/d"))
            {
                cmdInsertDs.Enabled = false;
                cmdUpdateDs.Enabled = true;
                cmdDeleteDs.Enabled = true;
            }
        }

        private void formatGrid()
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i].RowState == DataRowState.Unchanged)
                {
                    dgView.Rows[i].DefaultCellStyle.BackColor = Color.White;
                }
                else if (ds.Tables[0].Rows[i].RowState == DataRowState.Added)
                {
                    dgView.Rows[i].DefaultCellStyle.BackColor = Color.Green;
                }
                else if (ds.Tables[0].Rows[i].RowState == DataRowState.Modified)
                {
                    dgView.Rows[i].DefaultCellStyle.BackColor = Color.Blue;
                }
                else if (ds.Tables[0].Rows[i].RowState == DataRowState.Deleted) 
                {
                    dgView.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                }
            }
        }
    }
}
